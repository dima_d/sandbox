package foo

import java.io.Serializable

/**
 * TODO: Add javadoc
 */
class Neuron constructor(/** Количество дендритов */ private val dendritCount: Int) : Serializable {

    /** Взвешенная сумма сигналов дендритов */
    private var dendritsSumm: Double = 0.0

    /** Вес дендритов */
    private val dendritWeights = DoubleArray(dendritCount) {
        //dendritWeights[i] = Math.random()<0.5 ? Math.random()*0.3+0.6 : -Math.random()*0.3-0.6;
//        if (Math.random() < 0.5)
//            Math.random() * 0.3 + 15 / dendritCount
//        else
//            -Math.random() * 0.3 - 15 / dendritCount

        (Math.random()*2 - 1)
    }

    //-1 нейрону смещения не нужна ошибка
    private val errors = DoubleArray(dendritCount - 1) {0.0}

    /** Ошибка нейрона */
    private var error: Double = 0.0

    /** Сохраненные сигмоидные сигналы */
    private var sigmIn: DoubleArray? = null

    /** Входной сигнал нейрона смещения */
    private var biasIn: Double = 0.toDouble()



    /**
     * Получает сигналы на дендриты с нейронов предыдущего слоя
     * @param dendSygn сигмоидные сигналы на дендриты
     * @param bias сигнал нейрона смещения
     */
    fun takeDendSygnals(dendSygn: DoubleArray, bias: Double) {
        //+1 так как сигналы дендритов направляются без учета нейрона смещение
        if (dendSygn.size + 1 != dendritCount) throw Exception("NotMatchNeyronSygnCount")
        sigmIn = dendSygn
        biasIn = bias
        dendritsSumm = 0.0
        //важно чтобы перебирались входные сигналы
        dendSygn.indices.forEach {
            dendritsSumm += dendSygn[it] * dendritWeights[it]
        }
        dendritsSumm += bias * dendritWeights[dendritCount - 1]
    }

    /**
     * Сигмоидный сигнал
     * @return сигмоиду сигнала
     */
    fun giveSigmSignal(): Double {

        val d = when {
            dendritsSumm > 7 -> 1.0
            dendritsSumm in 2..7 -> 0.04 * dendritsSumm + 0.72
            dendritsSumm in -2..2 -> 0.19 * dendritsSumm + 0.5
            dendritsSumm in -7..-2 -> 0.0238 * dendritsSumm + 0.1666
            else -> 0.0
        }
        System.out.printf("%+3.3f -- %+3.3f",d,d*(1-d))
        println()
        return d




//        return 1 / (1 + Math.exp(-dendritsSumm))
    }

    /**
     * Принимает ошибку
     * @param err ошибка
     */
    fun takeError(err: Double) {
        error = err
    }

    /**
     * Раздает ошибки
     * @return ошибки
     */
    fun giveErrors(): DoubleArray {//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        //-1 нейрону смещения не нужна ошибка
        for (i in 0 until dendritCount - 1) {
            errors[i] = error * dendritWeights[i]
        }
        return errors
    }

    /**
     * Исправляет веса
     * @param learnCoef коефициент обучения
     */
    fun fixWeight(learnCoef: Double) {
        //-1 потому что нейрон смещение исправляется отдельно
        val giveSigmSignal = giveSigmSignal()
        for (i in 0 until dendritCount - 1) {
            var delta = sigmIn!![i] *
                    learnCoef *
                    giveSigmSignal *
                    (1 - giveSigmSignal) *
                    error

//            if(delta>=0)
//                delta*=(1 - giveSigmSignal)
//            else
//                delta*=giveSigmSignal

            dendritWeights[i] += delta
        }
        dendritWeights[dendritCount - 1] += biasIn * learnCoef * giveSigmSignal * (1 - giveSigmSignal) * error
    }


    /**
     * Раcпечатывает состояние нейрона
     */
    fun printNeyron() {
//        println("Dendrit count - " + dendritCount)
        var cnt = 0
        for (dendritWeight in dendritWeights) {
            System.out.print(String.format(" %+2.4f ",dendritWeight))
            cnt++
        }
    }
}
