package foo

class Timber {

    companion object {
        fun d(log:String){
            System.out.println("${Thread.currentThread().getName()}  $log")
        }

        fun d(log: Throwable?) {
            System.out.println(log)
        }

        fun d(log: String, tag: String) {
            System.out.println("$tag: $log")
        }
    }
}
