package foo

import java.io.Serializable
import javax.print.attribute.standard.MediaSize

/**
 * Конструктор нейронной сети
 * @param sens количество сенсорных нейронов
 * @param networkMap карта нейронной сети
 */
class NeuralNetwork(val sensorCount: Int,val networkMap: IntArray) : Serializable {


    /** Массив слоёв */
    private val layers: Array<Layer> = Array(networkMap.size) {
        when (it) {
            0 -> Layer(networkMap[0], sensorCount)
            else -> Layer(networkMap[it], networkMap[it - 1])
        }
    }


    /** Получает ответ сети по указанному заданию
     * @param task задание
     * @return  */
    fun getAnswer(task: DoubleArray): DoubleArray {
        if (task.size != sensorCount) throw Exception("NotMatchNeyronSygnCount")

        layers[0].acceptSygnals(task)
        for (i in 1 until networkMap.size)
            layers[i].acceptSygnals(layers[i - 1].giveSygnals())

        return layers[layers.size - 1].giveSygnals()
    }

    /** Тренировка нейронной сети
     * @param task подборка заданий
     * @param answ подборка ответов
     * @param learnCoef коефициент обучения
     * @param shureness уверенность сети */
    fun trainNeuralNetwork(task: Array<DoubleArray>?, answ: Array<DoubleArray>, learnCoef: Double, shureness: Double) {
        if (task!!.size != answ.size) throw Exception("NotMatchNeyronSygnCount")
        if (task.any { it.size != sensorCount }) throw Exception("NotMatchNeyronSygnCount")

        var glError: Boolean
        //int cykles = 0;
        var totalErr: Double
        do {
            glError = false
            totalErr = 0.0
            for (i in task.indices) {

                val errors = answ[i] - getAnswer(task[i])

                totalErr += errors.sumByDouble { Math.abs(it) }
                if (errors.any { Math.abs(it) > shureness }) {
                    backpropagateAndFix(errors, learnCoef)
                    glError = true
                }
            }
//            System.out.print("\r total error - " + totalErr)
            // TODO updateMessage("number - " + totalErr)
        } while (glError)

        System.out.println()
    }




    /** Обратное распространение ошибок
     * @param errs ошибки */
    private fun backPropagateErrors(errs: DoubleArray) {
        layers[networkMap.size - 1].acceptErrors(errs)
        for (i in networkMap.size - 1 downTo 1) {
            layers[i - 1].acceptErrors(layers[i].giveErrors())
        }
    }

    /** Исправление весов
     * @param learnCoef коефициент обучения */
    private fun fixWeights(learnCoef: Double) = layers.forEach { it.fixWeights(learnCoef) }

    /** Обратное распространение ошибок и исправление весов
     * @param errs ошибки
     * @param learnCoef коефициент обучения */
    private fun backpropagateAndFix(errs: DoubleArray, learnCoef: Double) {
        backPropagateErrors(errs)
        fixWeights(learnCoef)
    }

    /** Распечатывает слой нейронов */
    fun printNN() {
        println("Layers count - " + networkMap.size)
        println("sensors count - " + sensorCount)
        var count = 0
        layers.forEach { layer ->
            println("")
            println("Layer #" + count)
            layer.printLayer()
            count++
        }
    }

}

private operator fun DoubleArray.minus(other: DoubleArray):DoubleArray =  DoubleArray(size, {this[it] - other[it]})
