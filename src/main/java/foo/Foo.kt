package foo

import io.reactivex.Observable

data class Name(var name: String)

fun iter(array:Array<Int>) {
    array[array.size - 1] += 1
    ((array.size - 1) downTo 0).forEach { x ->
        if (array[x] > 2) {
            array[x] = 0
            if (x > 0) {
                array[x - 1] += 1
            }
        }
    }
}

private fun check(n: Int): Int {
    val array = Array<Int>(n) { 0 }

    var count = 0
    (0 until Math.pow(3.0, n.toDouble()).toInt()).forEach {
        iter(array)
        val answear = array
                .mapIndexed { index: Int, i: Int ->
                    when (i) {
                        0 -> (index == 0) || (array[index - 1] == 1) || (array[index - 1] == 2)
                        1 -> (index == 0) || (array[index - 1] == 0) || (array[index - 1] == 2)
                        2 -> index != 0 && index != array.size - 1 && ((array[index - 1] == 0 && array[index + 1] == 1) || (array[index - 1] == 1 && array[index + 1] == 0))
                        else -> false
                    }
                }.all { it }

        if (answear) count++

        if (answear)
            System.out.println(array.joinToString())
    }

    return count
}

fun main(arg: Array<String>) {

    (1..6).forEach {
        System.out.println("$it -- ${check(it)}")
    }



//    1
//    0
//    1 -- 2
//    0, 1
//    1, 0
//    2 -- 2
//    1, 0, 1
//    0, 1, 0
//    0, 2, 1
//    1, 2, 0
//    3 -- 4
//    0, 1, 0, 1
//    1, 0, 1, 0
//    1, 0, 2, 1
//    0, 1, 2, 0
//    0, 2, 1, 0
//    1, 2, 0, 1
//    4 -- 6
//    0, 1, 0, 1, 0
//    0, 1, 0, 2, 1
//    0, 1, 2, 0, 1
//    0, 2, 1, 0, 1
//    0, 2, 1, 2, 0
//    1, 0, 1, 0, 1
//    1, 0, 1, 2, 0
//    1, 0, 2, 1, 0
//    1, 2, 0, 1, 0
//    1, 2, 0, 2, 1
//    5 -- 10

    //            doubleArrayOf(1.0, 0.0, 0.0, 0.0),


//    while (true) {
//    }

//
//    val nn1 = NeuralNetwork(4, intArrayOf(3, 2, 1))
//
//    nn1.printNN()
//
//    //выборка с определённой логикой (я уже забыл условия но сеть всеравно даёт правильный ответ :)))
//    //можете закомментировать на свой вкус любые одну или две пары (ответ-вопрос), сеть справляется.
//    val task1 = arrayOf(
//            doubleArrayOf(1.0, 0.0, 0.0, 0.0),
//            doubleArrayOf(0.0, 1.0, 0.0, 0.0),
//            doubleArrayOf(1.0, 1.0, 0.0, 0.0),
//            doubleArrayOf(0.0, 0.0, 1.0, 0.0),
//            doubleArrayOf(1.0, 0.0, 1.0, 0.0),
//            doubleArrayOf(0.0, 1.0, 1.0, 0.0),
//            doubleArrayOf(1.0, 1.0, 1.0, 0.0),
//            doubleArrayOf(0.0, 0.0, 0.0, 1.0),
//            //{1.0,0.0,0.0,1.0},
//            //{0.0,1.0,0.0,1.0},
//            doubleArrayOf(1.0, 1.0, 0.0, 1.0),
//            doubleArrayOf(0.0, 0.0, 1.0, 1.0)
//    )
//
//    //ответы
//    val answ1 = arrayOf(
//            doubleArrayOf(1.0),
//            doubleArrayOf(0.0),
//            doubleArrayOf(0.0),
//            doubleArrayOf(0.0),
//            doubleArrayOf(0.0),
//            doubleArrayOf(1.0),
//            doubleArrayOf(1.0),
//            doubleArrayOf(1.0),
//            //{1.0},//ожидаемый ответ 1
//            //{0.0},//ожидаемый ответ 2
//
//            doubleArrayOf(0.0),
//            doubleArrayOf(0.0))
//
//
//    nn1.trainNeuralNetwork(task1, answ1, 0.5, 0.05)
//
//    nn1.printNN()
//
//    System.out.println("Ожидаемый ответ 1 - 1")
//    System.out.println("Ожидаемый ответ 2 - 0")
//    System.out.println("Ответ 1 - ${nn1.getAnswer(doubleArrayOf(1.0, 0.0, 0.0, 1.0))[0]}")
//    System.out.println("Ответ 2 - ${nn1.getAnswer(doubleArrayOf(0.0, 1.0, 0.0, 1.0))[0]}")

//    for (i in 1..70) {
//        if (i % 3 == 0)
//            System.out.print("Fuzz")
//        if (i % 5 == 0)
//            System.out.print("Buzz")
//        if (i % 5 != 0 && i % 3 != 0)
//            System.out.print(i)
//
//        System.out.println()
//
//
//    }

}






