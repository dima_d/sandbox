package foo

import neuralNetwork.NeuralNerwork
import neuralNetwork.ariphmetics.minus
import neuralNetwork.neurons.Input
import neuralNetwork.neurons.Neuron
import org.decimal4j.immutable.Decimal10f
import java.util.prefs.Preferences


/**
 * TODO: Add javadoc
 */
fun test(){

    val task1 = arrayOf(
            arrayOf(Decimal10f.ONE, Decimal10f.ZERO, Decimal10f.ZERO, Decimal10f.ZERO),
            arrayOf(Decimal10f.ZERO, Decimal10f.ONE, Decimal10f.ZERO, Decimal10f.ZERO),
            arrayOf(Decimal10f.ONE, Decimal10f.ONE, Decimal10f.ZERO, Decimal10f.ZERO),
            arrayOf(Decimal10f.ZERO, Decimal10f.ZERO, Decimal10f.ONE, Decimal10f.ZERO),
            arrayOf(Decimal10f.ONE, Decimal10f.ZERO, Decimal10f.ONE, Decimal10f.ZERO),
            arrayOf(Decimal10f.ZERO, Decimal10f.ONE, Decimal10f.ONE, Decimal10f.ZERO),
            arrayOf(Decimal10f.ONE, Decimal10f.ONE, Decimal10f.ONE, Decimal10f.ZERO),
            arrayOf(Decimal10f.ZERO, Decimal10f.ZERO, Decimal10f.ZERO, Decimal10f.ONE),
            //{Decimal10f.ONE,Decimal10f.ZERO,Decimal10f.ZERO,Decimal10f.ONE},
            //{Decimal10f.ZERO,Decimal10f.ONE,Decimal10f.ZERO,Decimal10f.ONE},
            arrayOf(Decimal10f.ONE, Decimal10f.ONE, Decimal10f.ZERO, Decimal10f.ONE),
            arrayOf(Decimal10f.ZERO, Decimal10f.ZERO, Decimal10f.ONE, Decimal10f.ONE)
    )

    //ответы
    val answ1 = arrayOf(
            arrayOf(Decimal10f.ONE),
            arrayOf(Decimal10f.ZERO),
            arrayOf(Decimal10f.ZERO),
            arrayOf(Decimal10f.ZERO),
            arrayOf(Decimal10f.ZERO),
            arrayOf(Decimal10f.ONE),
            arrayOf(Decimal10f.ONE),
            arrayOf(Decimal10f.ONE),
            //{Decimal10f.ONE},//ожидаемый ответ 1
            //{Decimal10f.ZERO},//ожидаемый ответ 2
            arrayOf(Decimal10f.ZERO),
            arrayOf(Decimal10f.ZERO))



    val nn = NeuralNerwork(4, arrayListOf(3,2,1))


    nn.train(task1, answ1, Decimal10f.valueOf(0.5), Decimal10f.valueOf(0.05))

    System.out.println("Ожидаемый ответ 1 - 1")
    System.out.println("Ожидаемый ответ 2 - 0")
    System.out.println("Ответ 1 - ${nn.getAnswer(arrayOf(Decimal10f.ONE, Decimal10f.ZERO, Decimal10f.ZERO, Decimal10f.ONE))[0]}")
    System.out.println("Ответ 2 - ${nn.getAnswer(arrayOf(Decimal10f.ZERO, Decimal10f.ONE, Decimal10f.ZERO, Decimal10f.ONE))[0]}")

}

fun test1(){


}