package foo

import java.io.Serializable

/**
 * Конструктор слоя
 * @param neuCount количество нейронов в слое
 * @param prewNeyCount количество нейронов в предыдущем слое
 */
class Layer
(
        /** Количество нейронов в слое */
        private val neyronCount: Int,
        /** Количество нейронов в предыдущем слое */
        private val prewNeuronCount: Int)
    : Serializable {

    /** Массив нейронов слоя */
    private val neyrons: Array<Neuron> = Array(neyronCount) {
        //+1 дендрит на нейрон зміщення
        Neuron(prewNeuronCount + 1)
    }

    /** Нейрон смещения */
    private val bias = if (Math.random() < 0.5) -1.0 else 1.0
    private val sygnals = DoubleArray(neyronCount)



    /** Отправляет сигналы нейронов слоя(текущего) в следующий слой
     * @return сигналы нейронов */
    fun giveSygnals(): DoubleArray {
        for (i in 0 until neyronCount) {
            sygnals[i] = neyrons[i].giveSigmSignal()
        }
        /*int count=0;
        for (double sygnal : sygnals) {
            System.out.println("neyron #"+count+" sygn - "+sygnal);
            count++;
        }*/
        return sygnals
    }

    /** Принимает сигналы предыдущего слоя
     * @param sygnals сигналы предыдущего слоя */
    fun acceptSygnals(sygnals: DoubleArray) {
        //if(sygnals.length!=neyrons.length) throw  Exception("NotMatchNeyronSygnCount")
        for (neyron in neyrons) {
            neyron.takeDendSygnals(sygnals, bias)
        }
    }

    /** Принимает ошибки
     * @param errs ошибки */
    fun acceptErrors(errs: DoubleArray?) {
        if (neyrons.size != errs?.size) throw Exception("NotMatchNeyronSygnCount")
        for (i in 0 until neyronCount) {
            neyrons[i].takeError(errs[i])
        }
    }

    /** Передаёт ошибки следующему слою
     * @return ошибки */
    fun giveErrors(): DoubleArray {
        /*double[][] layErr = new double[neyronCount][];
        for (int i = 0; i < neyronCount; i++) {
            layErr[i]=neyrons[i]?.giveErrors();
        }
        return layErr;*/
        val layErrs = DoubleArray(prewNeuronCount)
        for (i in 0 until prewNeuronCount) {
            for (j in 0 until neyronCount) {
                layErrs[i] += neyrons[j].giveErrors()[i]
            }
        }
        return layErrs
    }

    /** Исправляет веса
     * @param learnCoef коефициент обучения */
    fun fixWeights(learnCoef: Double) = neyrons.forEach { neyron -> neyron.fixWeight(learnCoef) }

    /** Метод распечатывает слой */
    fun printLayer() {
//        println("neyCount - " + neyronCount)
        println("bias - " + bias)
//        var cnt = 0
        for (neyron in neyrons) {
//            println("Neyron #" + cnt)
            neyron.printNeyron()
            println("")
//            cnt++
        }
    }

}