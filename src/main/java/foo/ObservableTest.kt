package foo

import com.akaita.java.rxjava2debug.RxJava2Debug
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers

fun run() {
    RxJava2Debug.enableRxJava2AssemblyTracking()

    Observable.just("event")
            .subscribeOn(Schedulers.computation())
//            .doOnEvent { _, _ -> System.out.println("HandledException Start") }
            .map { null }
            .observeOn(Schedulers.single())
            .subscribe({ it -> System.out.println("HandledException Subscribe") }, {RxJava2Debug.getEnhancedStackTrace(it).printStackTrace()})
//            .subscribeWithFallback ({ it -> System.out.println("HandledException Subscribe") })

    while (true) {
    }

}