package ru.tensor.sbis.common.util.statemachine

import foo.Timber
import foo.log
import io.reactivex.Flowable
import io.reactivex.Observable
import io.reactivex.Scheduler
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.processors.BehaviorProcessor
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.PublishSubject
import ru.tensor.sbis.common.rx.plusAssign
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.CopyOnWriteArrayList
import java.util.concurrent.TimeUnit
import kotlin.reflect.KClass

/**
 * Интерфейс создания событий для машины состояний. Может быть простым или дата классом, содержать любую логику или хранить данные.
 * Жизненный цикл события короткий, событие не рекомендуется сохранять где либо. При каждом вызове рекомендую создавать новый инстанс объекта.
 */
interface SessionEvent

/**
 * Интерфейс событий установки общих стейтов для машины состояний. Может быть простым или дата классом, содержать любую логику или хранить данные.
 * Жизненный цикл события короткий, событие не рекомендуется сохранять где либо. При каждом вызове рекомендую создавать новый инстанс объекта.
 */
interface SessionStateEvent : SessionEvent

/**
 * Базовый интерфейс машины состояний
 */
interface StateMachine {
    /**
     * Синхронный вызов события событие всегда летит в текущее состояние.
     */
    fun fire(event: SessionEvent)

    /**
     * Остановка и очистка памяти стейт машины. Объект больше не может быть использован.
     */
    fun stop()
}

/**
 * Интерфейс реализации машины состояний для Котлин делегации.
 */
interface StateMachineInner : StateMachine {
    /**
     * Включение подробного логирования переходов по состояниям с соответствующим тегом.
     * Только для отладки, не добрасывать в основной код
     */
    @Deprecated("Использовать только для отладки, удалять перед мерджем!", ReplaceWith(" // отладочный лог удален"))
    fun enableLogging(tag: String)

    /**
     * Установка состояния
     */
    fun setState(state: SessionState)

    /**
     * Установка состояния, отложенного на определенное время.
     * Удобно для тайм-аутов. Любая установка стейта прервет ожидание и отменит это состояние.
     */
    fun setState(state: SessionState, time: Long, timeUnit: TimeUnit)

    /**
     * Немедленно выполнить все отложенные состояния.
     */
    fun executePendingNow()

    /**
     * Обзервер, потока состояний.
     */
    val currentStateObservable: Flowable<SessionState>

    /**
     * Получить текущее состояние.
     */
    fun currentState(): SessionState?

    /**
     * Установка реакции на событие, общего для всей машины состояний. Используется в основном для изменения стейтов.
     * Выполняется на потоке выбранном в конструкторе стейт машины.
     * одно и тоже событие невозможно обрабатывать дважды. Последующие обработчики затрут предыдущие.
     */
    fun <T : SessionStateEvent> state(clazz: KClass<T>, consumer: (t: T) -> Unit): ((t: Any) -> Unit)?
}

/**
 * Класс-шаблон для описания состояний.
 */
abstract class SessionState {
    @Volatile
    internal var stateMachine: StateMachineInner? = null
    protected val disposer = CompositeDisposable()
    private val map = ConcurrentHashMap<KClass<out SessionEvent>, (t: Any) -> Unit>()
    internal val onSetListeners = CopyOnWriteArrayList<() -> Unit>()
    internal fun <T : SessionEvent> invokeEvent(event: T) = (map[event::class] as? (t: T) -> Unit)?.invoke(event)

    /**
     * Установка шаблона вызываемый при установке состояния. Можно устанавливать много раз (например при наследовании).
     * Выполняется на потоке выбранном в конструкторе стейт машины.
     */
    fun onSet(set: () -> Unit) = onSetListeners.add(set)

    /**
     * Установка состояния
     */
    protected fun setState(state: SessionState) = stateMachine?.setState(state)

    /**
     * Установка состояния, отложенного на определенное время.
     * Удобно для тайм-аутов. Любая установка стейта прервет ожидание и отменит это состояние.
     */
    protected fun setState(state: SessionState, time: Long, timeUnit: TimeUnit) = stateMachine?.setState(state, time, timeUnit)


    /**
     * Установка реакции на событие
     * Выполняется на потоке выбранном в конструкторе стейт машины.
     * одно и тоже событие невозможно обрабатывать дважды. Последующие обработчики затрут предыдущие.
     */
    @Suppress("UNCHECKED_CAST")
    fun <T : SessionEvent> event(clazz: KClass<T>, consumer: (t: T) -> Unit) =
        map.put(clazz, consumer as (t: Any) -> Unit)

    /**
     * Ожидание события с тайм-аутом. Если событие приходит в течении установленного времени, обрабатывается как обычно, таймер останавливается
     * В противном случае вызывается  onTimeout.
     * одно и тоже событие невозможно обрабатывать дважды. Последующие обработчики затрут предыдущие (включая установленные методом event).
     */
    fun <T : SessionEvent> timeout(
        time: Long,
        timeUnit: TimeUnit,
        event: KClass<T>,
        onEvent: ((t: T) -> Unit)? = null,
        onTimeout: (() -> Unit)? = null
    ) {
        val timer = BehaviorSubject.createDefault(time)
        event(event) {
            timer.onNext(time)
            onEvent?.invoke(it)
        }

        disposer += timer
            .switchMap { Observable.timer(it, timeUnit) }
            .doOnNext { onTimeout?.invoke() }
            .subscribe()
    }

    internal fun stop() {
        disposer.dispose()
        stateMachine = null
    }

    fun fire(event: SessionEvent) = stateMachine?.fire(event)
        ?: throw RuntimeException("This state is not active in state machine")
}

/**
 * Реализация машины состояний.
 * @param statesScheduler поток на котором выполняется установка стейтов. По умолчанию mainThread()
 * @param eventsScheduler поток на котором выполняется обработка событий. По умолчанию io()
 */
class StateMachineImpl(
    private val statesScheduler: Scheduler = Schedulers.computation(),
    private val eventsScheduler: Scheduler = Schedulers.io()
) :
    StateMachineInner {

    private val disposer = CompositeDisposable()
    private val events = PublishSubject.create<SessionEvent>()
    private val states = BehaviorProcessor.create<SessionState>()
    override val currentStateObservable = states
    private var tag: String? = null
    private val pendingExecutor = PublishSubject.create<Long>()
    private val timeoutDisposable = CompositeDisposable()
    private val map = ConcurrentHashMap<KClass<out SessionEvent>, (t: Any) -> Unit>()

    init {
        disposer += events.observeOn(eventsScheduler).subscribe(::invokeEvent)
    }

    private fun <T : SessionEvent> invokeEvent(event: T) {
        tag?.let { Timber.d("%s  Event ${event.javaClass.simpleName} called for state ${states.value?.javaClass?.simpleName} ${System.identityHashCode(this)} ", it) }

        if (event is SessionStateEvent) (map[event::class] as? (t: T) -> Unit)?.invoke(event)
        else states.value?.invokeEvent(event)
    }

    @Suppress("OverridingDeprecatedMember")
    override fun enableLogging(tag: String) {
        this.tag = tag
    }

    override fun stop() {
        tag?.let { Timber.d("%s stop() called", it); }
        disposer.dispose()
        events.onComplete()
        states.onComplete()
    }

    @Suppress("UNCHECKED_CAST")
    override fun <T : SessionStateEvent> state(clazz: KClass<T>, consumer: (t: T) -> Unit) =
        map.put(clazz, consumer as (t: Any) -> Unit)

    override fun setState(state: SessionState) {
        tag?.let { Timber.d("%s  State ${state.javaClass.simpleName} set ${System.identityHashCode(this)}", it); }
        states.value?.stop()
        state.stateMachine = this
        states.onNext(state)

         Observable.just(state)
            .observeOn(statesScheduler)
            .map { state -> state.onSetListeners.forEach { it.invoke()} }
            .blockingFirst()

        timeoutDisposable.clear()
        tag?.let { Timber.d("%s  State ${state.javaClass.simpleName} set finish ${System.identityHashCode(this)}", it); }
    }

    override fun currentState() = states.value

    override fun setState(state: SessionState, time: Long, timeUnit: TimeUnit) {
        timeoutDisposable += Observable.timer(time, timeUnit)
            .mergeWith(pendingExecutor)
            .firstElement()
            .subscribe { setState(state) }
    }

    override fun executePendingNow() = pendingExecutor.onNext(0)

    override fun fire(event: SessionEvent) {
        events.onNext(event)
    }


}



