package ru.tensor.sbis.common.util.statemachine

import io.reactivex.ObservableSource
import io.reactivex.Observer
import io.reactivex.disposables.Disposable
import io.reactivex.exceptions.Exceptions
import io.reactivex.internal.disposables.DisposableHelper
import io.reactivex.internal.disposables.EmptyDisposable
import io.reactivex.internal.functions.ObjectHelper
import io.reactivex.internal.observers.QueueDrainObserver
import io.reactivex.internal.queue.MpscLinkedQueue
import io.reactivex.internal.util.QueueDrainHelper
import java.util.concurrent.Callable

class PausableBufferExactBoundaryObserver<T, U : MutableList<T>, B>(
    actual: Observer<in U>,
    private val bufferSupplier: Callable<U>,
    private val valve: ObservableSource<B>
) : QueueDrainObserver<T, U, U>(
    actual,
    MpscLinkedQueue<U>()
), Observer<T>, Disposable {

    lateinit var upstreamDisposable: Disposable

    lateinit var otherDisposable: Disposable

    var buffer: U? = null

    override fun onSubscribe(disposable: Disposable) {
        if (DisposableHelper.validate(this.upstreamDisposable, disposable)) {
            this.upstreamDisposable = disposable


            try {
                buffer = ObjectHelper.requireNonNull(
                    bufferSupplier.call(),
                    "The buffer supplied is null"
                )
            } catch (e: Throwable) {
                Exceptions.throwIfFatal(e)
                cancelled = true
                disposable.dispose()
                EmptyDisposable.error(e, downstream)
                return
            }

            val bs = PausableBufferBoundaryObserver(this)
            otherDisposable = bs

            downstream.onSubscribe(this)

            if (!cancelled) {
                valve.subscribe(bs)
            }
        }
    }

    override fun onNext(t: T) {
        synchronized(this) {
            (buffer ?: return).add(t)
        }
    }

    override fun onError(t: Throwable) {
        dispose()
        downstream.onError(t)
    }

    override fun onComplete() {
        val b: U?
        synchronized(this) {
            b = buffer
            if (b == null) {
                return
            }
            buffer = null
        }
        queue.offer(b!!)
        done = true
        if (enter()) {
            QueueDrainHelper.drainLoop(queue, downstream, false, this, this)
        }
    }

    override fun dispose() {
        if (!cancelled) {
            cancelled = true
            otherDisposable.dispose()
            upstreamDisposable.dispose()

            if (enter()) {
                queue.clear()
            }
        }
    }

    override fun isDisposed(): Boolean {
        return cancelled
    }

    operator fun next() {

        val next: U

        try {
            next = ObjectHelper.requireNonNull(
                bufferSupplier.call(),
                "The buffer supplied is null"
            )
        } catch (e: Throwable) {
            Exceptions.throwIfFatal(e)
            dispose()
            downstream.onError(e)
            return
        }

        val b: U?
        synchronized(this) {
            b = buffer
            if (b == null) {
                return
            }
            buffer = next
        }

        fastPathEmit(b, false, this)
    }

    override fun accept(a: Observer<in U>?, v: U?) {
        downstream.onNext(v)
    }

}