package ru.tensor.sbis.common.util.statemachine

import io.reactivex.Observable
import io.reactivex.ObservableSource
import io.reactivex.Observer
import io.reactivex.internal.fuseable.HasUpstreamObservableSource
import io.reactivex.internal.util.ArrayListSupplier
import io.reactivex.observers.DisposableObserver
import io.reactivex.observers.SerializedObserver
import io.reactivex.plugins.RxJavaPlugins
import java.util.concurrent.Callable

fun <T> Observable<T>.pausable(valve: Observable<Boolean>): Observable<MutableList<T>> {


    return RxJavaPlugins.onAssembly(PausableBufferExactBoundary<T, MutableList<T>, Boolean>(this, valve, ArrayListSupplier.asCallable()))
}



class PausableBufferExactBoundary<T, U : MutableList<T>, B>(
    val source: ObservableSource<T>,
    internal val valve: ObservableSource<B>,
    internal val bufferSupplier: Callable<U>
) : Observable<U>(),
    HasUpstreamObservableSource<T> {
    override fun source(): ObservableSource<T> =
        source

    override fun subscribeActual(t: Observer<in U>) {
        source.subscribe(
            PausableBufferExactBoundaryObserver(
                SerializedObserver(t),
                bufferSupplier,
                valve
            )
        )
    }

}

class PausableBufferBoundaryObserver<T, U : MutableList<T>, B>(val parent: PausableBufferExactBoundaryObserver<T, U, B>) :
    DisposableObserver<B>() {

    override fun onNext(t: B) {
        parent.next()
    }

    override fun onError(t: Throwable) {
        parent.onError(t)
    }

    override fun onComplete() {
        parent.onComplete()
    }
}
