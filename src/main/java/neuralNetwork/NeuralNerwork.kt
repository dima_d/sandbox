package neuralNetwork

import neuralNetwork.layers.InputLayer
import neuralNetwork.layers.NeuronLayer
import org.decimal4j.immutable.Decimal10f

/**
 * TODO: Add javadoc
 */
class NeuralNerwork(inputs: Int, layersMap: List<Int>) {

    val input = InputLayer(inputs)
    val layers = ArrayList<NeuronLayer>(layersMap.size)

    init {
        layers.add(NeuronLayer(layersMap[0], input))
        layersMap.drop(1).forEach { layers.add(NeuronLayer(it, layers.last())) }
    }

    fun getAnswer(array: Array<Decimal10f>): Array<Decimal10f> {
        input.input(array)
        layers.forEach { it.process() }
        return layers.last().answer()
    }

    fun backPropagation(teatherResult: Array<Decimal10f>, learnCoef: Decimal10f, shureness: Decimal10f): Boolean {
        val lastLayer = layers.last().layer
        teatherResult.forEachIndexed { index, it -> lastLayer[index].correctResult(it) }

        if (lastLayer.any { it.error.abs() > shureness }) {
            layers
                    .reversed()
                    .flatMap { it.layer.asList() }
                    .forEach { it.fixError(learnCoef) }
            return false
        }
        return true
    }

    fun train(task: Array<Array<Decimal10f>>, answ: Array<Array<Decimal10f>>, learnCoef: Decimal10f, shureness: Decimal10f) {
        var error = false
        do {

            error = true
            task.forEachIndexed { index, it ->
                getAnswer(it)
                error = backPropagation(answ[index], learnCoef, shureness) && error
            }
        } while (!error)
    }

    override fun toString(): String = layers.toString()

}