package neuralNetwork.neurons

import neuralNetwork.ActivationFunc
import neuralNetwork.ariphmetics.BicubicSygma
import neuralNetwork.IDendrit
import neuralNetwork.INeuron
import neuralNetwork.ariphmetics.minus
import neuralNetwork.ariphmetics.times
import neuralNetwork.ariphmetics.unaryMinus
import org.decimal4j.immutable.Decimal10f
import org.decimal4j.mutable.MutableDecimal10f


class Neuron(
        input: Array<out IDendrit>?,
        val activationFunc: ActivationFunc = BicubicSygma()
) : INeuron {
    private val synapses = Array(input?.size?:0) { Synaps(input!!.get(it)) }

    override var value: Decimal10f = Decimal10f.ZERO
    private var synapsesSumm = Decimal10f.ZERO
    override var error = Decimal10f.ZERO

    override fun fixError(learnCoef: Decimal10f) {
        val sygmoidError = error * learnCoef * activationFunc.derivative(synapsesSumm)
        synapses.forEach { it.fixSynaps(-sygmoidError) }
        error = Decimal10f.ZERO
    }

    override fun process() {
        val sum = MutableDecimal10f()
        synapses.forEach { sum.add(it.value) }
        synapsesSumm = sum.toImmutableDecimal()
        value = activationFunc.process(synapsesSumm)
    }

    override fun correctResult(result: Decimal10f) {
        error = value - result
    }

    override fun toString(): String  = "N: value = $value error = $error  synapses = ${synapses.contentToString()}"
}


