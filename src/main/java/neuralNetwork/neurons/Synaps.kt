package neuralNetwork.neurons

import neuralNetwork.IDendrit
import neuralNetwork.ISynaps
import neuralNetwork.ariphmetics.plus
import neuralNetwork.ariphmetics.times
import org.decimal4j.immutable.Decimal10f

class Synaps(
        override val dendrit: IDendrit,
        override var weight: Decimal10f = Decimal10f.valueOf(Math.random() * 2 - 1)) : ISynaps {
    override val value get() = dendrit.value * weight
    override fun fixSynaps(sygmoidError: Decimal10f) {
        weight += dendrit.value * sygmoidError
        dendrit.error += sygmoidError * weight
    }

    override fun toString(): String  = weight.toString()

}