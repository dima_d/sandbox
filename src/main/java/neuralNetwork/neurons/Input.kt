package neuralNetwork.neurons

import neuralNetwork.IDendrit
import org.decimal4j.immutable.Decimal10f

class Input: IDendrit {
    override var error: Decimal10f get() = Decimal10f.ZERO; set (value) {}
    override var value = Decimal10f.ZERO
    override fun fixError(learnCoef: Decimal10f) {}
    override fun correctResult(result: Decimal10f) {}
    override fun toString(): String  = "input: value = $value"
}
