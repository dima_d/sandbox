package neuralNetwork.neurons

import neuralNetwork.INeuron
import org.decimal4j.immutable.Decimal10f

/** Нейрон смещения */
class Bias : INeuron {
    override var error: Decimal10f get() = Decimal10f.ZERO; set (value) {}
    override val value = Decimal10f.ONE
    override fun fixError(learnCoef: Decimal10f) {}
    override fun process() {}
    override fun correctResult(result: Decimal10f){}
}