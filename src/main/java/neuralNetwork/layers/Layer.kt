package neuralNetwork.layers

import neuralNetwork.IDendrit
import neuralNetwork.INeuron
import neuralNetwork.neurons.Bias
import neuralNetwork.neurons.Input
import neuralNetwork.neurons.Neuron
import org.decimal4j.immutable.Decimal10f

/**
 * TODO: Add javadoc
 */
interface Layer<T : IDendrit> {
    var layer: Array<T>
}

data class InputLayer(override var layer: Array<Input>) : Layer<Input> {
    constructor(i: Int) : this(Array(i) { Input() })
    fun input(array: Array<Decimal10f>){
        array.forEachIndexed{ index: Int, it: Decimal10f ->
            layer[index].value = it
        }
    }

    override fun toString(): String = layer.toString()
}

data class NeuronLayer(override var layer: Array<INeuron>) : Layer<INeuron> {
    constructor(i: Int, prev: Layer<out IDendrit>?) : this(Array(i+1) {
        when (it) {
            i -> Bias()
            else -> Neuron(prev?.layer)
        }
    })
    fun process() = layer.forEach { it.process() }
    fun answer() = layer.map { it.value }.toTypedArray()
    override fun toString(): String = layer.toString()
}