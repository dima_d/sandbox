package neuralNetwork.ariphmetics

import org.decimal4j.immutable.Decimal10f

/**
 * TODO: Add javadoc
 */
operator fun Decimal10f.times(other: Decimal10f): Decimal10f = this.multiply(other)
operator fun Decimal10f.div(other: Decimal10f): Decimal10f = this.divide(other)
operator fun Decimal10f.plus(other: Decimal10f): Decimal10f = this.add(other)
operator fun Decimal10f.minus(other: Decimal10f): Decimal10f = this.subtract(other)
operator fun Decimal10f.unaryMinus(): Decimal10f = this.negate()

