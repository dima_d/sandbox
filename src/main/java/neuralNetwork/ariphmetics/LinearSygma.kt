package neuralNetwork.ariphmetics

import neuralNetwork.ActivationFunc
import org.decimal4j.immutable.Decimal10f

/** particular - linear approcsimation of sigma function 1 / (1 + Math.exp(-x)) **/
class LinearSygma : ActivationFunc {
    @Suppress("ObjectPropertyName")
    companion object {
        val _7 = Decimal10f.valueOf(7)!!
        val _2 = Decimal10f.valueOf(2)!!
        val _0_5 = Decimal10f.valueOf(0.5)!!
        val _0_19 = Decimal10f.valueOf(0.19)!!
        val _0_04 = Decimal10f.valueOf(0.04)!!
        val _0_72 = Decimal10f.valueOf(0.72)!!
        val _0_0238 = Decimal10f.valueOf(0.0238)!!
        val _0_1666 = Decimal10f.valueOf(0.1666)!!
        val _m2 = Decimal10f.valueOf(-2)!!
        val _m7 = Decimal10f.valueOf(-7)!!
    }

    var cashed_in = Decimal10f.ONE
    var cashed_out = Decimal10f.ONE

    override fun process(x: Decimal10f): Decimal10f {
        cashed_in = x
        cashed_out = when {
            x > _7 -> Decimal10f.ONE
            x in _2.._7 -> _0_04 * x + _0_72
            x in _m2.._2 -> _0_19 * x + _0_5
            x in _m7.._m2 -> _0_0238 * x + _0_1666
            else -> Decimal10f.ZERO
        }
        return cashed_out
    }

    override fun derivative(x: Decimal10f): Decimal10f =
            if (cashed_in == x) {
                cashed_out * (Decimal10f.ONE - cashed_out)
            } else {
                val out = process(x)
                out * (Decimal10f.ONE - out)
            }

}