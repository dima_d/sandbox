package neuralNetwork.ariphmetics

import neuralNetwork.ActivationFunc
import org.decimal4j.immutable.Decimal10f
import org.decimal4j.immutable.Decimal10f.*

/**
 * bicubic interpolation of sigma function 1 / (1 + Math.exp(-x)) **/
class BicubicSygma : ActivationFunc {
    @Suppress("ObjectPropertyName")
    companion object {
        val _10 = Decimal10f.valueOf(10)!!
        val _4 = Decimal10f.valueOf(4)!!
        val _0 = Decimal10f.valueOf(0)!!
        val _m4 = Decimal10f.valueOf(-4)!!
        val _m10 = Decimal10f.valueOf(-10)!!

        val _0_0030 = Decimal10f.valueOf(0.0030)!!
        val _0_9700 = Decimal10f.valueOf(0.9700)!!
        val _0_027 = Decimal10f.valueOf(0.027)!!
        val _4_2 = Decimal10f.valueOf(4.2)!!
        val _0_982 = Decimal10f.valueOf(0.982)!!
        val _0_018 = Decimal10f.valueOf(0.018)!!
        val _0_0300 = Decimal10f.valueOf(0.0300)!!
    }

    var cashed_in = ONE
    var cashed_out = ONE

    override fun process(x: Decimal10f): Decimal10f {
        cashed_in = x
        cashed_out = when {
            x > _10 -> ONE
            x in _4.._10 -> _0_0030 * x + _0_9700
            x in _0.._4 -> -_0_027 * ((x - _4_2).pow(2)) + _0_982
            x in _m4.._0 -> _0_027 * ((x + _4_2).pow(2)) + _0_018
            x in _m10.._m4 -> _0_0030 * x + _0_0300
            x < _10 -> ZERO
            else -> ZERO
        }
        return cashed_out
    }

    override fun derivative(x: Decimal10f): Decimal10f =
            if (cashed_in == x) {
                cashed_out * (ONE - cashed_out)
            } else {
                val out = process(x)
                out * (ONE - out)
            }

}