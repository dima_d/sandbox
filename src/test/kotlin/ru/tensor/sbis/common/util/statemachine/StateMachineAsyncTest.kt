package ru.tensor.sbis.common.util.statemachine


import foo.log
import io.reactivex.schedulers.Schedulers
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

internal class StateMachineAsyncTest {

    internal class DisabledStateEvent : SessionStateEvent
    internal class EventDisable : SessionEvent
    class DisabledState() : SessionState(){
        init {
            event(EventEnable::class) { setState(EnabledState()) }
        }
    }

    internal class EnabledStateEvent : SessionStateEvent
    internal class EventEnable : SessionEvent
    class EnabledState() : SessionState(){
        init {
            event(EventDisable::class) { setState(DisabledState()) }
        }
    }


    internal class StateEvent3 : SessionStateEvent

    class StateMachineTest:StateMachineInner by StateMachineImpl(Schedulers.computation(), Schedulers.newThread()){
        init {
            enableLogging("test")
            setState(DisabledState())
        }
    }


    /**
     * https://online.sbis.ru/opendoc.html?guid=b45cff7e-ad78-4fc3-a121-b2eba0ec9bcc
     */
    @Test
    fun `стейт машина верно переключает состояния при синхронной работе`() {
        val stateMashine = StateMachineTest()

        val testSubscriber = stateMashine.currentStateObservable
            .subscribeOn ( Schedulers.computation() )
            .log("stateMashine.currentStateObservable")
            .test()


        stateMashine.fire(EventEnable())
        stateMashine.fire(EventDisable())


        testSubscriber.assertNoErrors()
            .awaitCount(3)
            .assertValueAt(0){it is DisabledState}
            .assertValueAt(1){it is EnabledState}
            .assertValueAt(2){it is DisabledState}
//            .assertComplete()

    }


}