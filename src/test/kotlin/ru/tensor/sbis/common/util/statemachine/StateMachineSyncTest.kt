@file:Suppress("NonAsciiCharacters")

package ru.tensor.sbis.common.util.statemachine


import com.nhaarman.mockitokotlin2.mock
import foo.log
import io.reactivex.schedulers.Schedulers
import org.junit.jupiter.api.Test
import org.mockito.Mockito.times
import org.mockito.Mockito.verify

internal class StateMachineSyncTest {

    internal class DisabledStateEvent : SessionStateEvent
    internal class EventDisable : SessionEvent
    class DisabledState() : SessionState() {
        init {
            event(EventEnable::class) { fire(EnabledStateEvent()) }
        }
    }

    internal class EnabledStateEvent : SessionStateEvent
    internal class EventEnable : SessionEvent
    open class EnabledState() : SessionState() {
        init {
            event(EventDisable::class) { fire(DisabledStateEvent()) }
        }
    }

    class StateMachineTest : StateMachineInner by StateMachineImpl(Schedulers.trampoline(), Schedulers.trampoline()) {
        init {
            enableLogging("test")
            state(DisabledStateEvent::class) { setState(DisabledState()) }
            state(EnabledStateEvent::class) { setState(EnabledState()) }
            setState(DisabledState())
        }
    }

    internal class TestEvent : SessionEvent


    @Test
    fun `стейт машина верно переключает состояния`() {
        val stateMashine = StateMachineTest()

        val testSubscriber = stateMashine.currentStateObservable
            .subscribeOn(Schedulers.computation())
            .test()


        stateMashine.fire(EventEnable())
        stateMashine.fire(EventDisable())
        stateMashine.stop()

        testSubscriber.assertNoErrors()
            .assertValueAt(0) { it is DisabledState }
            .assertValueAt(1) { it is EnabledState }
            .assertValueAt(2) { it is DisabledState }
            .assertComplete()

    }


    @Test
    fun `верный порядок иницианизации стейта`() {

        val setFunction = mock<() -> Unit>()
        val eventFunction = mock<(t: Any) -> Unit>()

        val newState = object : SessionState() {
            init {
                onSet(setFunction)
                event(TestEvent::class, eventFunction)
            }
        }

        val stateMashine =
            object : StateMachineInner by StateMachineImpl(Schedulers.trampoline(), Schedulers.trampoline()) {
                init {
                    state(EnabledStateEvent::class) { setState(newState) }
                    setState(DisabledState())
                }
            }

        stateMashine.fire(EventEnable())

        val event = TestEvent()
        stateMashine.fire(event)

        verify(setFunction, times(1)).invoke()
        verify(eventFunction, times(1)).invoke(event)

    }
}