package neuralNetwork.ariphmetics

import neuralNetwork.ariphmetics.BicubicSygma
import org.decimal4j.immutable.Decimal10f
import org.junit.Assert
import org.junit.Test

class BicubicSygmaTest{
    @Test fun process(){
        val test = BicubicSygma()

        for (i:Long in -100L .. 100L) {

            val v = test.process(Decimal10f.valueOf(i)).toDouble() - (1 / (1 + Math.exp(-(i.toDouble()))))
//            System.out.println(v)
            Assert.assertTrue("Test in x = $i return value ${Math.abs(v)} thats >0.05 ", Math.abs(v)<0.05)
        }

    }

    @Test
    fun derivative() {
        val test = BicubicSygma()

        for (i:Long in -10L .. 10L) {
            val exp:Double =  (1 / (1 + Math.exp(-(i.toDouble()))))

            val derivative = exp * (1- exp)

            val v = test.derivative(Decimal10f.valueOf(i)).toDouble() - derivative
//            System.out.println(v)
            Assert.assertTrue("Test in x = $i return value ${Math.abs(v)} thats >0.01 ", Math.abs(v)<0.05)
        }

    }
}