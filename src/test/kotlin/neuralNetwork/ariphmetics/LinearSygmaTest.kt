package neuralNetwork.ariphmetics

import org.decimal4j.immutable.Decimal10f
import org.junit.Assert
import org.junit.Test

import org.junit.Assert.*

/**
 * TODO: Add javadoc
 */
class LinearSygmaTest {

    @Test
    fun process() {
        val test = LinearSygma()

        for (i:Long in -100L .. 100L) {

            val v = test.process(Decimal10f.valueOf(i)).toDouble() - (1 / (1 + Math.exp(-(i.toDouble()))))
//            System.out.println(v)
            Assert.assertTrue("Test in x = $i return value ${Math.abs(v)} thats >0.1 ", Math.abs(v)<0.3)
        }
    }

    @Test
    fun derivative() {

        val test = LinearSygma()

        for (i:Long in -10L .. 10L) {
            val exp:Double =  (1 / (1 + Math.exp(-(i.toDouble()))))

            val derivative = exp * (1- exp)


            val v = test.derivative(Decimal10f.valueOf(i)).toDouble() - derivative
//            System.out.println(v)
            Assert.assertTrue("Test in x = $i return value ${Math.abs(v)} thats >0.01 ", Math.abs(v)<0.1)
        }

    }
}