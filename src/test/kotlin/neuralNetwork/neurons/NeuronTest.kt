package neuralNetwork.neurons

import neuralNetwork.ariphmetics.minus
import neuralNetwork.ariphmetics.plus
import org.decimal4j.immutable.Decimal10f
import org.junit.Assert
import org.junit.Assert.*
import org.junit.Test

/**
 * TODO: Add javadoc
 */
class NeuronTest{
    /**
     * Тренирует сеть из 1 нейрона
     */
    @Test fun `simple train`(){
        val input = Input()
        val neuron = Neuron(arrayOf(input))
        input.value = Decimal10f.valueOf(Math.random()*10 -5)
        val result = Decimal10f.valueOf(Math.random())
        var counter = 0
        do {
            counter++
            neuron.process()
            neuron.correctResult(result)
            neuron.fixError(Decimal10f.valueOf(0.5))
            Assert.assertTrue(" Тест на тренировку 1 нейрона привысил число попыток $counter $input, outpur:$result, neuron:$neuron", counter<200)
        } while ( (neuron.value - result).abs()  > Decimal10f.valueOf(0.05))

        System.out.println("Test passed in $counter : $neuron, input:$input target: $result")
    }

    @Test fun `simple train 2 inputs`(){
        val input = Input().apply { value =  Decimal10f.valueOf(Math.random()*10 -5) }
        val input2 = Input().apply { value =  Decimal10f.valueOf(Math.random()*10 -5) }
        val neuron = Neuron(arrayOf(input, input2))
        val result = Decimal10f.valueOf(Math.random())
        var counter = 0
        do {
            counter++
            neuron.process()
            neuron.correctResult(result)
            neuron.fixError(Decimal10f.valueOf(0.5))
            Assert.assertTrue(" Тест на тренировку 1 нейрона привысил число попыток $counter $input, output:$result, neuron:$neuron", counter<200)
        } while ( (neuron.value - result).abs()  > Decimal10f.valueOf(0.05))

        System.out.println("Test passed in $counter : $neuron, input:$input target: $result")
    }


    @Test fun `simple train 2 neurons`(){
        val input = Input()
        val neuron1 = Neuron(arrayOf(input))
        val neuron2 = Neuron(arrayOf(neuron1))

        val value = Decimal10f.valueOf(Math.random()*10 -5)
        val result = Decimal10f.valueOf(Math.random())
        System.out.println(" $value, $result" )

        var counter = 0
        var error: Decimal10f
        do {
            error = Decimal10f.ZERO
            counter++

            input.value = value
            neuron1.process()
            neuron2.process()
            neuron2.correctResult(result)
            System.out.print(" n2 $neuron2" )
            neuron2.fixError(Decimal10f.valueOf(0.5))
            System.out.println(" n1 $neuron1" )
            neuron1.fixError(Decimal10f.valueOf(0.5))

            error += (neuron2.value - result).abs()

            Assert.assertTrue(" Тест на тренировку 2 нейронов привысил число попыток $counter ${value}, outpur:${result}, neuron1:$neuron1, neuron2:$neuron2", counter<10000)
        } while ( error  > Decimal10f.valueOf(0.05))

        System.out.println("Test passed in $counter : $neuron1, $neuron2 input:$value target: $result")
    }




}